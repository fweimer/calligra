# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Adi Spivak, 2012.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: calligra_shape_picture\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-25 00:18+0000\n"
"PO-Revision-Date: 2017-05-16 06:38-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: ChangeImageCommand.cpp:26
#, fuzzy
msgctxt "(qtundo-format)"
msgid "Change image"
msgstr "החלף צלמית"

#: ChangeImageCommand.cpp:43
#, fuzzy
msgctxt "(qtundo-format)"
msgid "Crop image"
msgstr "החלף צלמית"

#: ChangeImageCommand.cpp:57
#, fuzzy
msgctxt "(qtundo-format)"
msgid "Change image color mode"
msgstr "החלף צלמית"

#: ClipCommand.cpp:18
msgctxt "(qtundo-format)"
msgid "Contour image (by image analysis)"
msgstr ""

#: ClipCommand.cpp:20
msgctxt "(qtundo-format)"
msgid "Remove image contour"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, bnImageFile)
#: forms/wdgPictureTool.ui:20
#, kde-format
msgid "Replace image..."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbAspect)
#: forms/wdgPictureTool.ui:32
#, kde-format
msgid "Keep proportions"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, leftLabel)
#: forms/wdgPictureTool.ui:58
#, kde-format
msgid "Left:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, rightLabel)
#: forms/wdgPictureTool.ui:68
#, kde-format
msgid "Right:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, bottomLabel)
#: forms/wdgPictureTool.ui:91
#, kde-format
msgid "Bottom:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, topLabel)
#: forms/wdgPictureTool.ui:111
#, kde-format
msgid "Top:"
msgstr ""

#. i18n: ectx: property (text), widget (QToolButton, bnFill)
#: forms/wdgPictureTool.ui:131
#, kde-format
msgid "Reset"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbContour)
#: forms/wdgPictureTool.ui:148
#, kde-format
msgid "Contour (by image analysis)"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: forms/wdgPictureTool.ui:178
#, kde-format
msgid "Color mode:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: forms/wdgPictureTool.ui:193
#, kde-format
msgid "Crop"
msgstr ""

#: PictureShapeFactory.cpp:31
#, kde-format
msgid "Image"
msgstr "צלמית"

#: PictureShapeFactory.cpp:33
#, kde-format
msgid "Image shape that can display jpg, png etc."
msgstr "צורת צלמית שיכולה להציג jpg, png וכדומה."

#: PictureTool.cpp:95
#, kde-format
msgid "Standard"
msgstr ""

#: PictureTool.cpp:96
#, kde-format
msgid "Greyscale"
msgstr ""

#: PictureTool.cpp:97
#, kde-format
msgid "Monochrome"
msgstr ""

#: PictureTool.cpp:98
#, kde-format
msgid "Watermark"
msgstr ""

#: PictureToolFactory.cpp:19
#, fuzzy, kde-format
msgid "Picture editing"
msgstr "כלי עריכת תמונה"
