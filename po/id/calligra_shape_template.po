# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the calligra package.
# Wantoyo <wantoyek@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: calligra\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-25 00:18+0000\n"
"PO-Revision-Date: 2019-07-09 13:45+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ChangeSomethingCommand.cpp:34
msgctxt "(qtundo-format)"
msgid "Change something"
msgstr "Ubah sesuatu"

#: TemplateShapeFactory.cpp:32
#, kde-format
msgid "Template shape"
msgstr "Bentuk templat"

#: TemplateShapeFactory.cpp:34
#, kde-format
msgid "Simple shape that is used as a template for developing other shapes."
msgstr ""
"Bentuk sederhana yang digunakan sebagai templat untuk mengembangkan bentuk "
"lain."

#: TemplateTool.cpp:77
#, kde-format
msgid "Open"
msgstr "Buka"

#: TemplateToolFactory.cpp:26
#, kde-format
msgid "Template shape editing"
msgstr "Pengeditan bentuk templat"
